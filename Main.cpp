#include <iostream>
#include <conio.h>
using namespace std;

float original;

// math methods
float Add(float a, float b) {
	return a + b;
}
float Subtract(float a, float b) {
	return a - b;
}
float Multiply(float a, float b) {
	return a * b;
}
bool Divide(float a, float b, float& result) {
	if (b == 0) {
		return false;
	}
	else {
		result = a / b;
	}
}
// decrements the int until it is 1 or 0
float Pow(float a, int b) {

	if (b <= 0) {
		return 1;
	}
	if (b == 1) {
		return a;
	}

	if (b > 1) {
		return Pow(a*=original, b-1);
	}
	
}



// main method
int main() {

	float primary; // user input variables
	float secondary;

	char operation; // type of operation

	float result = 0; // result after math

	bool running = true; // looping the calculator
	char again; // if the application should run again

	while (running) {

		std::cout << "Enter two values to do math with\nFirst: ";

		cin >> primary; // get first user input

		// ask for second input
		std::cout << "Second: ";

		// get second input
		cin >> secondary;

		// ask and get operation
		std::cout << "Choose operation: [+, -, *, /, ^] ";
		cin >> operation;

		// switch case to check the operation and act accordingly
		switch (operation) {
		case('+'): {
			result = Add(primary, secondary);
			break;
		}

		case('-'): {
			result = Subtract(primary, secondary);
			break;
		}
		case('*'): {
			result = Multiply(primary, secondary);
			break;
		}
		case('/'): {
			if (!Divide(primary, secondary, result)) {
				std::cout << "Cannot divide by zero!\n";
			}
			break;
		}
		case('^'): {
			original = primary;
			result = Pow(primary, (int)secondary);
			break;
		}
		}

		std::cout << "Result: " << result;
		std::cout << "\nPerform another calculation? [y/n]\n";
		cin >> again;
		if (again == 'n') {
			running = false;
		}
	}

	std::cout << "Press any button to exit ";

	_getch();
	// main method must end with a return.
	return 0;
}